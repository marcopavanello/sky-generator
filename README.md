# Sky Generator
This is a sky generator that calculates a physically based sky based on an improved version of [Nishita](https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/simulating-sky/simulating-colors-of-the-sky) 1993 single scattering model.


### Install
You need to have the following packages installed:  
**python** 3.8 or a later version, **pip**, **numpy**.

On Linux just run the following command for your distribution to install them:
* **Ubuntu**: ``` sudo apt install python3 python3-pip && pip3 install numpy ```
* **Fedora**: ``` sudo dnf install python3 python3-pip python3-numpy ```
* **Arch Linux**: ``` sudo pacman -S python python-pip python-numpy ```


### Run
Run the code with ``` python3 main.py ```.  
(note it may be just ``` python ``` for some distributions)


### How it works
When finishing, the program will show the rendered sky image with the OS image viewer.
To change the sky parameters, just change them in the `properties.py` file, there you will find the sun rotation, camera position altitude along with other settings.
